import random

def isPrime(numberOfSecretParts):
    if numberOfSecretParts < 2:
        return False
    i = 2
    while i*i <= numberOfSecretParts:
        if numberOfSecretParts % i == 0:
            return False
        i += 1
    return True

def primeNumbers(fr, to):
    primes = []
    for i in range(fr, to):
        if isPrime(i):
            primes.append(i)
    assert len(primes) != 0
    return primes

def primitiveRootCheck(g, numberOfSecretParts):
    powers=[]
    for i in range(1, numberOfSecretParts):
        powers.append(pow(g, i, numberOfSecretParts))
    for i in range(1, numberOfSecretParts):
        if powers.count(i) > 1:
            return False
        return True
    
def searchG(numberOfSecretParts):
    flag = False
    g = None
    while (not flag):
        g = random.randint(2, numberOfSecretParts-1)
        if (primitiveRootCheck(g, numberOfSecretParts)):
            flag = True
    return g

def main():
    fr = 10000
    to = 20000
    primes = primeNumbers(fr, to)
    numberOfSecretParts = random.choice(primes)
    print(f"numberOfSecretParts: {numberOfSecretParts}")
    g = searchG(numberOfSecretParts)
    print(f"g: {g}")
    x = random.randint(fr, to)
    y = random.randint(fr, to)
    Ax = pow(g, x, numberOfSecretParts)
    By = pow(g, y, numberOfSecretParts)
    Ak = pow(By, x, numberOfSecretParts)
    Bk = pow(Ax, y, numberOfSecretParts)
    print(f"Ax: {Ax}, By: {By}")
    print(f"Ak: {Ak}, Bk: {Bk}")

if __name__ == "__main__":
    main()