import random
 
def isPrime(numberOfSecretParts):
    if numberOfSecretParts < 2:
        return False
    i = 2
    while i*i <= numberOfSecretParts:
        if numberOfSecretParts % i == 0:
            return False
        i += 1
    return True

def primeNumbers(digits):
    primes = []
    min = pow(10, digits - 1)
    max = pow(10, digits) - 1
    for i in range(min, max):
        if isPrime(i):
            primes.append(i)
    return primes
    
def nwd(a, b):
    if b > 0:
        return nwd(b, a%b)
    else:
        return a
    
def encrypt(rawMessage, public):
    return [pow(ord(char), public.first, public.second) for char in rawMessage]

def decrypt(encodeMessage, private):
    return ''.join(chr(pow(ord, private.first, private.second)) for ord in encodeMessage)

class Key:
    def __init__(self, first, second):
        self.first = first
        self.second = second

def main():
    digits = 4
    primes = primeNumbers(digits)
    randomPrimes = random.choices(primes, k=2)
    p = randomPrimes[0]
    q = randomPrimes[1]
    print(p, q)
    print(isPrime(p), isPrime(q))
    numberOfSecretParts = p*q
    phi = (p-1)*(q-1)
    flag = False
    e = None
    while (not flag):
        e = random.choice(primes)
        if (nwd(e, numberOfSecretParts) == 1 and e!=p and e!=q):
            flag = True
    print(phi, e, nwd(phi, e))
    d = pow(e, -1, phi)
    print(d)

    public = Key(e, numberOfSecretParts)
    private = Key(d, numberOfSecretParts)

    rawMessage = "Witam oto moja niezaszyfrowana wiadomosc. Do zobaczenia."
    print(rawMessage)
    encodeMessage = encrypt(rawMessage, public)
    print(encodeMessage)
    decodeMessage = decrypt(encodeMessage, private)
    print(decodeMessage)

if __name__ == "__main__":
    main()
