from PIL import Image

def loadImage(path):
    im = Image.open(path, 'r').convert('RGB')
    width, height = im.size
    pixel_values = list(im.getdata())
    return (width, height, pixel_values)

def convertMessageToBits(message):
    return ''.join(format(ord(i), '08b') for i in message)

def convertIntToBits(rgb):
    return "{:08b}".format(rgb)

def base2ToBase10(string):
    suma = 0
    iter = 0
    string = string[::-1]
    for i in range(len(string)):
        suma += int(string[i]) * pow(2, iter)
        iter += 1
    return suma

def changeRGB(messageBits, data, position):
    rgbPixel = data[position]
    rgbTmp = []
    for i in range(len(messageBits)):
        if i%3 == 0 and i != 0:
            data[position] = (rgbTmp[0], rgbTmp[1], rgbTmp[2])
            rgbTmp.clear()
            position += 1
            rgbPixel = data[position]
        color = convertIntToBits(rgbPixel[i%3])
        color = color[0:len(color)-1]
        color += messageBits[i]
        color = base2ToBase10(color)
        rgbTmp.append(color)
        if i == len(messageBits)-1 and len(rgbTmp) > 0:
            missing = 3 - len(rgbTmp) # 0 or 1 or 2
            if missing == 0:
                data[position] = (rgbTmp[0], rgbTmp[1], rgbTmp[2])
            elif missing == 1:
                data[position] = (rgbTmp[0], rgbTmp[1], rgbPixel[2])
            elif missing == 2:
                data[position] = (rgbTmp[0], rgbPixel[1], rgbPixel[2])

def saveImage(data, width, height, path):
    im = Image.new('1', (width, height)).convert('RGB')
    for i in range(width):
        for j in range(height):
            im.putpixel((j, i), data[i*width + j])
    im.save(path)

def readsLastBits(data, width, height):
    lastBits = ""
    for i in range(width):
        for j in range(height):
            rgbPixel = data[i*width + j]
            lastBits += "1" if rgbPixel[0]%2 == 1 else "0"
            lastBits += "1" if rgbPixel[1]%2 == 1 else "0"
            lastBits += "1" if rgbPixel[2]%2 == 1 else "0"
    return lastBits

def readMessage(imgPath):
    msg = ""
    width, height, data = loadImage(imgPath)
    lastBits = readsLastBits(data, width, height)
    for i in range(int(len(lastBits)/8)):
        msg += chr(int(lastBits[8*i:8*i+8], 2))
    return msg

def main():
    width, height, data = loadImage("./lab7/rawImage.jpg")
    rawMessage = "Hello there! General Kenobi."
    messageBits = convertMessageToBits(rawMessage)
    # for every char in message we need 8 bits to switch, in RGB that will be 3 pixels (2 pixels + 3RG from 3rd)
    # whole message cannot be longer than:
    assert len(messageBits) <= width * height * 3
    print(f'rawMessage: {rawMessage}\nmessageBits: {messageBits}')
    startPosition = 0
    changeRGB(messageBits, data, startPosition)
    saveImage(data, width, height, "./lab7/cryptoImage.png")
    encryptedMessage = readMessage("./lab7/cryptoImage.png")
    encryptedMessage = encryptedMessage[0:len(rawMessage)]
    print(encryptedMessage)

if __name__ == "__main__":
    main()