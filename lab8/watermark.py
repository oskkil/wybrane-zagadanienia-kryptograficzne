from PIL import Image
import random

def loadImage(path):
    im = Image.open(path, 'r').convert('RGB')
    width, height = im.size
    pixel_values = list(im.getdata())
    return (width, height, pixel_values)

def cutRange(val, offset):
    return (((val) * (255 - offset - (0 + offset))) / (255 - 0)) + 0 + offset

def cutData(data, offset):
    for pixel in data:
        pixel = (
            cutRange(pixel[0], offset),
            cutRange(pixel[1], offset),
            cutRange(pixel[2], offset)
        )

def getLuminosity(pixel):
    return (0.2126*pixel[0]) + (0.7152*pixel[1]) + (0.0722*pixel[2])

def saveImage(data, width, height, path):
    im = Image.new('1', (width, height)).convert('RGB')
    for i in range(width):
        for j in range(height):
            im.putpixel((j, i), data[i*width + j])
    im.save(path)

def Sn(data, n, randomIntegers):
    suma = 0
    for i in range(n):
        a = getLuminosity(data[randomIntegers[2*i]])
        b = getLuminosity(data[randomIntegers[2*i+1]])
        suma += (a-b)
    return suma

def SnPrim(data, n, delta, randomIntegers):
    suma = 2*delta*n
    for i in range(n):
        a = getLuminosity(data[randomIntegers[2*i]])
        b = getLuminosity(data[randomIntegers[2*i+1]])
        suma += (a-b)
    return suma

def addWaterMark(data, n, delta, randomIntegers):
    for i in range(n):
        pixel1 = data[randomIntegers[2*i]]
        data[randomIntegers[2*i]] = (
            min(255, pixel1[0] + delta),
            min(255, pixel1[1] + delta),
            min(255, pixel1[2] + delta)
        )
        pixel2 = data[randomIntegers[2*i+1]]
        data[randomIntegers[2*i+1]] = (
            max(0, pixel2[0] - delta),
            max(0, pixel2[1] - delta),
            max(0, pixel2[2] - delta)
        )

def detectWaterMark(watermarkData, n, delta, snCheck, randomIntegers):
    for i in range(n):
        pixel1 = watermarkData[randomIntegers[2*i]]
        watermarkData[randomIntegers[2*i]] = (
            max(0, pixel1[0] - delta),
            max(0, pixel1[1] - delta),
            max(0, pixel1[2] - delta)
        )
        pixel2 = watermarkData[randomIntegers[2*i+1]]
        watermarkData[randomIntegers[2*i+1]] = (
            min(255, pixel2[0] + delta),
            min(255, pixel2[1] + delta),
            min(255, pixel2[2] + delta)
        )
    sn = Sn(watermarkData, n, randomIntegers)
    print(snCheck, sn)
    if sn == snCheck:
        return True
    return False

def generate_random_integers(start, end, n, seed=None):
    if seed is not None:
        random.seed(seed)
    return [random.randint(start, end) for _ in range(n)]

def main():
    width, height, data = loadImage("./lab8/orange.jpg")
    delta = 5
    cutData(data, delta)
    minimum = 0
    maximum = width*height-1
    seed = 12345
    n = 20
    randomIntegers = generate_random_integers(minimum, maximum, 2*n, seed)
    sn = Sn(data, n, randomIntegers)
    addWaterMark(data, n, delta, randomIntegers)
    snprim = SnPrim(data, n, delta, randomIntegers)
    print(sn, snprim)
    #saveImage(data, width, height, "./lab8/cryptoImage.png")
    isWaterMarked = detectWaterMark(data, n, delta, sn, randomIntegers)
    print(isWaterMarked)

if __name__ == "__main__":
    main()