import random
 
def isPrime(numberOfSecretParts):
    if numberOfSecretParts < 2:
        return False
    i = 2
    while i*i <= numberOfSecretParts:
        if numberOfSecretParts % i == 0:
            return False
        i += 1
    return True
def primeNumbers():
    primes = []
    for i in range(1000, 10000):
        if (i%4 == 3 and isPrime(i)):
            primes.append(i)
    return primes
    
def nwd(a, b):
    if b > 0:
        return nwd(b, a%b)
    else:
        return a
def fillx(x0, numberOfSecretParts):
    x_list = [pow(x0, 2, numberOfSecretParts)]
    bits = [x_list[0]%2]
    for i in range(1, 20001):
        number = pow(x_list[i-1], 2, numberOfSecretParts)
        x_list.append(number)
        bits.append(number%2)
    return bits
def divide_chunks(l, numberOfSecretParts): 
    for i in range(0, len(l), numberOfSecretParts):  
        yield l[i:i + numberOfSecretParts]
def bitTo10(arr):
    suma = 0
    numberOfSecretParts = len(arr)-1
    for i in range(len(arr)):
        suma += arr[i] * pow(2, numberOfSecretParts)
        numberOfSecretParts-=1
    return suma

# TEST FUNCTIONS
def pojedynczych_bitow(bits):
    counter0 = 0
    counter1 = 0
    for bit in bits:
        if not bit:
            counter0 +=1
        else:
            counter1 +=1
    print("Test pojedynczych bitow")
    print(f"Needed range: (9725; 10275)\nCounter for 0: {counter0}\nCounter for 1: {counter1}")
    if (counter0 > 9725 and counter0 < 10275) and (counter1 > 9725 and counter1 < 10275):
        print("Passed!")
        return True
    else:
        print("Failed!")
        return False
def serii(bits):
    lenghts = [[0 for i in range(6)] for j in range(2)]
    current = bits[0]
    current_len = 1
    for i in range (1, len(bits)):
        if current == bits[i]:
            current_len+=1
        else:
            if current_len > 6:
                current_len = 6
            lenghts[current][current_len-1] += 1
            current = bits[i]
            current_len = 1
    print("Test serii")
    print(f"Needed ranges:\nLength 1: (2315;2685)\nLength 2: (527;1386)\nLength 3: (240;384)\nLength 4: (103;209)\nLength 5+: (103;209)")
    print(f"0/1 - length - count")
    for i in range(2):
        for j in range(6):
            print(f"{i} - {j+1} - {lenghts[i][j]}")
    flag = True
    for i in range(2):
        if ( \
            (lenghts[i][0] <= 2685 and lenghts[i][0] >= 2315) \
            and (lenghts[i][1] <= 1386 and lenghts[i][1] >= 1114) \
            and (lenghts[i][2] <= 723 and lenghts[i][2] >= 527) \
            and (lenghts[i][3] <= 384 and lenghts[i][3] >= 240) \
            and (lenghts[i][4] <= 209 and lenghts[i][4] >= 103) \
            and (lenghts[i][5] <= 209 and lenghts[i][5] >= 103) \
        ):
            continue
        else:
            flag = False
    if flag:
        print("Passed!")
        return True
    else:
        print("Failed!")
        return False
def dlugiejSerii(bits):
    print("Test dlugiej serii")
    counter = 0
    for i in range(len(bits)-1):
        if bits[i] == bits[i+1]:
            counter+=1
        else:
            counter=0
        if counter >= 26:
            print("Failed!")
            return False
    print("Passed!")
    return True
def pokerowy(bits):
    bits = list(divide_chunks(bits, 4))
    count_combinations = [0 for i in range(16)]
    for bitArr in bits:
        count_combinations[bitTo10(bitArr)]+=1
    suma = 0
    for i in range(len(count_combinations)):
        suma += pow(count_combinations[i],2)
    x = 16 / 5000 * suma - 5000
    print("Test pokerowy")
    print(f"Needed range: (2.16;46.17)")
    print(f"x: {x}")
    if (x < 46.17 and x > 2.16):
        print("Passed!")
        return True
    else:
        print("Failed!")
        return False
    
def tests(bits):
    pojedynczych_bitow(bits)
    serii(bits)
    dlugiejSerii(bits)
    pokerowy(bits)
    
def main():
    primes = primeNumbers()
    randomPrimes = random.choices(primes, k=2)
    p = randomPrimes[0]
    q = randomPrimes[1]
    # print(p, q)
    # print(p%4, q%4)
    # print(isPrime(p), isPrime(q))
    numberOfSecretParts = p*q
    flag = False
    while (not flag):
        x = random.randint(1000,9999)
        if (nwd(x, numberOfSecretParts) == 1):
            flag = True
    # print(x, numberOfSecretParts, nwd(x,numberOfSecretParts))
    x0 = pow(x, 2, numberOfSecretParts)
    # print(x0)
    bits = fillx(x0, numberOfSecretParts)
    # print(bits)

    #TESTS
    print("Basic values:")
    print(f"p: {p}, q: {q}, numberOfSecretParts: {numberOfSecretParts}, x: {x}, x0: {x0}")
    print("Important checks:")
    print(f"p%4: {p%4}, q%4: {q%4}, nwd(x,numberOfSecretParts): {nwd(x,numberOfSecretParts)}, isPrime(p): {isPrime(p)}, isPrime(q): {isPrime(q)}")


    print("\nTests\numberOfSecretParts")
    tests(bits)

if __name__ == "__main__":
    main()