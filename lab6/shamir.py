import random

def reconstructSecret(sample):
	sums = 0
	for i in range(len(sample)):
		product = 1
		for j in range(len(sample)):
			if i != j:
				product *= ( sample[j][0] / (sample[j][0] - sample[i][0]))
		product *= sample[i][1]
		sums += product
	return round(sums)

def polynom(x, parts):
	point = 0
	for partIdx, partVal in enumerate(parts[::-1]):
		point += x ** partIdx * partVal
	return point

def parts(minPartsToGetSecret, secret, max_val):
	parts = [random.randrange(0, max_val) for i in range(minPartsToGetSecret - 1)]
	parts.append(secret)
	return parts

def generateParts(numberOfSecretParts, minPartsToGetSecret, secret, max_val):
	genParts = parts(minPartsToGetSecret, secret, max_val)
	secretShares = []
	for i in range(1, numberOfSecretParts+1):
		x = random.randrange(1, max_val)
		secretShares.append((x, polynom(x, genParts)))
	return secretShares

def main():
	max_val = 10000
	minPartsToGetSecret = 3
	numberOfSecretParts = 5
	secret = 4932
	print("Secret: ", secret)
	parts = generateParts(numberOfSecretParts, minPartsToGetSecret, secret, max_val)
	print(f'Generated parts:\n{", ".join(str(part) for part in parts)}')
	randomSampleToRecreateSecret = random.sample(parts, minPartsToGetSecret)
	print(f'Random samples of parts:\n{", ".join(str(part) for part in randomSampleToRecreateSecret)}')
	print("Reconstructed secret: ", reconstructSecret(randomSampleToRecreateSecret))

if __name__ == '__main__':
    main()