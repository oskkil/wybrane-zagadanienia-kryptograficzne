import random
from PIL import Image

WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
choices = [(0,1),(1,0)]

def loadImage(path):
    im = Image.open(path, 'r').convert('RGB')
    width, height = im.size
    pixel_values = list(im.getdata())
    return (width, height, pixel_values)

def isWhite(pixel):
    return pixel == WHITE

def addSecondPanelPixel(pixel):
    if (isWhite(pixel)):
        u1 = random.choice(choices)
        u2 = choices[choices.index(u1)]
        return (u1, u2)
    else:
        u1 = random.choice(choices)
        choices.remove(u1)
        u2 = random.choice(choices)
        choices.append(u1)
        return (u1, u2)

def createVisual(width, height, data):
    u1 = []
    u2 = []
    for i in range(width):
        for j in range(height):
            u1_pixel, u2_pixel = addSecondPanelPixel(data[i*width + j])
            u1.append(u1_pixel)
            u2.append(u2_pixel)
    return (u1, u2)

def showSection(u, width, height, path):
    im = Image.new('1', (width*2, height)).convert('RGB')
    for i in range(width):
        iter = 0
        for j in range(height):
            pixel1, pixel2 = u[i*width + j]
            im.putpixel((iter, i), WHITE if pixel1 == 1 else BLACK)
            im.putpixel((iter+1, i), WHITE if pixel2 == 1 else BLACK)
            iter += 2
    im.save(path)

def putOnSections(u1, u2, width, height):
    lastSection = []
    for i in range(width):
        for j in range(height):
            if u1[i*width + j] == u2[i*width + j]:
                lastSection.append(u1[i*width + j])
            else:
                lastSection.append((0, 0))
    return lastSection

def main():
    width, height, data = loadImage("./lab5/image.jpg")
    # width, height, data = loadImage("./lab5/boat.png")
    assert width == height
    u1, u2 = createVisual(width, height, data)
    lastSection = putOnSections(u1, u2, width, height)
    showSection(u1, width, height, "./lab5/u1.png")
    showSection(u2, width, height, "./lab5/u2.png")
    showSection(lastSection, width, height, "./lab5/endSection.png")

if __name__ == "__main__":
    main()