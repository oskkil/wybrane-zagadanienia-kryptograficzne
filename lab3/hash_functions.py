# 1. szybkosc pliki 1MB, 5MB, 10 MB, tworzenie + szyfrowanie
# 2. SAC - XOR bitowy, zlicz 1 -> ilosc zmienionych bitow / dlugosc skrotu * 100% = (48-52)%
# 3. kolizje
# + sprawozdanie

import hashlib

def MD5(str_to_hash):
    return hashlib.md5(str_to_hash.encode()).hexdigest()

def SHA1(str_to_hash):
    return hashlib.sha1(str_to_hash.encode()).hexdigest()

def SHA2(str_to_hash):
    return hashlib.sha256(str_to_hash.encode()).hexdigest()

def SHA3(str_to_hash):
    return hashlib.sha3_256(str_to_hash.encode()).hexdigest()

def hashFunctions(str_to_hash):
    return (MD5(str_to_hash), SHA1(str_to_hash), SHA2(str_to_hash), SHA3(str_to_hash))

def checkDifferences(str1, str2):
    assert len(str1) == len(str2)
    changes = 0
    for i in range(len(str1)):
        if str1[i] != str2[i]:
            changes += 1
    return changes / len(str1)

def checkTuples(tuple1, tuple2):
    assert len(tuple1) == len(tuple2)
    changes = []
    for i in range(len(tuple1)):
        changes.append(checkDifferences(tuple1[i], tuple2[i]))
    return changes

def main():
    str1_to_hash = "Kot"
    str2_to_hash = "Kou"
    result_str1 = hashFunctions(str1_to_hash)
    result_str2 = hashFunctions(str2_to_hash)
    changes = checkTuples(result_str1, result_str2)
    print(changes)

if __name__ == "__main__":
    main()
